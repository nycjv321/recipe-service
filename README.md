## Checking out Project

    git checkout git@gitlab.com:nycjv321/recipe-service.git

## Setting up the Database

### Docker
Download Docker from: https://www.docker.com/

    docker run -p 5432:5432 --name recipes-datastore -d postgres

###  Create a terminal session against the container

    docker exec -it recipes-datastore /bin/sh

### Connect to the datbase process

    psql -U postgres

### Create the database

    create database recipes;

### Connect to the database

    \c recipes;

### Provision the UUID-OSSP Extensions

    CREATE EXTENSION "uuid-ossp";

### Quit and Exit the Terminal

    \q
    exit

## Videos

### [Hello world](https://youtu.be/qbVy8NC9_RI)

Run this command to checkout the starter project

    sbt -sbt-version 1.3.12 new http4s/http4s.g8 -b 0.21

### [Starting our Recipe Service](https://youtu.be/phTXi7mAde0)

The code at the end of this video exists on this branch `initial-recipe-service`:

    git clone feature/initial-recipe-service

### [Switching to a Real Database](https://youtu.be/WfkVul-xog8)

The code at the end of this video exists on this branch `an-actual-database`:

    git clone feature/an-actual-database


