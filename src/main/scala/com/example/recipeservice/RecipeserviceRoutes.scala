package com.example.recipeservice

import cats.effect.Sync
import cats.implicits._
import com.example.recipeservice.Recipes.{Recipe, RecipeMessage}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

object RecipeserviceRoutes {

  def recipeRoutes[F[_] : Sync](recipes: Recipes[F]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    HttpRoutes.of[F] {
      case request@POST -> Root / "recipes" =>
        for {
          recipe <- request.as[Recipe]
          createdRecipeE <- recipes.create(recipe)
          resp <- createdRecipeE match {
            case Left(message) => BadRequest(message)
            case Right(createdRecipe) => Created(createdRecipe)
          }
        } yield resp
      case GET -> Root / "recipes" / id =>
        for {
          resolvedRecipeO <- recipes.findById(id)
          resp <- resolvedRecipeO match {
            case Right(resolvedRecipe) => Ok(resolvedRecipe)
            case Left(recipeMessage@RecipeMessage(message)) if message.startsWith("recipe did not exist with following identifier") => NotFound(recipeMessage)
            case Left(recipeMessage) => BadRequest(recipeMessage)
          }
        } yield {
          resp
        }
    }
  }
}
