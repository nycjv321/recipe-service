package com.example.recipeservice

import cats.effect.{ExitCode, IO, IOApp}

object Main extends IOApp {
  def run(args: List[String]) =
    RecipeserviceServer.stream[IO].compile.drain.as(ExitCode.Success)
}
